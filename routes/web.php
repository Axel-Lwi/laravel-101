<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//Ini tugas web statis

// Route::get('/', function () {
//     return view('home');
// });

// Route::get('/register', function () {
//     return view('register');
// });

// Route::get('/welcome', function () {
//     return view('welcome');
// });

// //Route::get('/ngepost', 'RegisterController@ngepost'); 
// Route::post('/ngepost', 'RegisterController@ngepost'); 


//tugas template

// Route::get('/', function () {
//     return view('adminlte.tugas.root');
// });

// Route::get('/tables/data', function () {
//     return view('adminlte.tugas.data');
// });


Route::get('/pertanyaan', 'PertanyaanController@index');


Route::get('/pertanyaan/create', 'PertanyaanController@create');

Route::post('/pertanyaan', 'PertanyaanController@store');


route::get('/pertanyaan/{id}', 'PertanyaanController@show');



