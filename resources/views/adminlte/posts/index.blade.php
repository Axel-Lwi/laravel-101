@extend('adminlte.master')

@section('content')
    <div class = "mt-3 ml-3">

    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Daftar Pertanyaan</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table class="table table-bordered">
                  <thead>                  
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>judul pertanyaan</th>
                      <th>isi</th>
                      <th>action</th>
                      <th style="width: 40px">Label</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($posts as key => $post)
                        <tr>
                            <td> {{key + 1}}</td>
                            <td> {{$post -> $judul}}</td>
                            <td> {{$post -> $isi}}</td>
                            <td>
                                <a href="/posts/{{post->id}}" class ="btn btn-info btn-sm">show</a>
                            <td>
                        </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
              
            </div>
    </div> 

@endsection