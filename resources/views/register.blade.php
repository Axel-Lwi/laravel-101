<!DOCTYPE html>

<head>
    <title>Sanbercode | Form Sign UP</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/ngepost" method ="post">                                                       <!--setelah submit, ke welcome.html-->
    @csrf
        <label for "frontName"> First name:</label>
    <br><br> 
        <input type="text" name ="fnama" id="frontName">                                              <!--textfield nama depan-->
    <br><br>
        <label for "lastName"> Last name:</label>
    <br><br>
        <input type="text" name ="lnama" id="lastName">                                               <!--textfield nama belakang-->
    <br> <br>
        <label for "gender"> Gender :</label>
    <br><br>    
        <input type="radio" name="gender" value="M">Male                                <!--pilih gender male/female-->
    <br>
        <input type="radio" name="gender" value="F">Female
    <br>
        <input type="radio" name="gender" value="O">Other
    <br><br>
        <label for "nationality"> Nationality :</label>
    <br><br>
        <select id="nationality" name="nationality" >                                   <!--pilih kebangsaan-->
            <option value="indonesian">Indonesian</option>
            <option value="singaporean">Singaporean</option>
            <option value="malaysian">Malaysian</option>
            <option value="australian">Australian</option>
        </select>
    <br><br>
        <label for "spoken"> Language Spoken :</label>                  
    <br><br>
        <input type="checkbox" id="Indo" name="spoken1" value="Bahasa Indonesia">       <!--pilih bahasa-->
        <label for="spoken1"> Bahasa Indonesia</label>
    <br>
        <input type="checkbox" id="En" name="spoken2" value="English">
        <label for="spoken2">English</label>
    <br>
        <input type="checkbox" id="ot" name="spoken3" value="English">
        <label for="spoken3">Other</label>
    <br><br>
        <label for="bio"> Bio :</label>                 
    <br><br>
        <textarea rows="7"></textarea>                                                   <!--ruang untuk mengetik bio-->
    <br><br>
        <input type="submit" value="Sign Up">                                            <!--tombol sign up-->
    </form>
    
</body>
</html>