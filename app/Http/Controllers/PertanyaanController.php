<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class PertanyaanController extends Controller
{
    public function create(){
        return view('adminlte.posts.create');
    }

    public function index(){
        
    }

    public function store(Request $req){
       // dd($req->all());
        $query = DB::table('pertanyaan')->insert([
                "judul" => $req["judul"],
                "isi" => $req["isi"]
            ]);

            return redirect('/pertanyaan/create');
    }

    public function index(){
        $posts = DB::table('pertanyaan')->get();
        ll
        return view('adminlte.posts.index', compact('posts'));
    }

    public function show($id){
        $post = DB::table('pertanyaan')->where('id',$id)->first();
        return view('adminlte.posts.show', compact('post'));
    }

}
